import csv

ncurva = {}

with open('/home/fidel/GitLab/numero-curva/nc_usv_suelo.csv', newline='') as csvfile:
    reader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in reader:
        ncurva[row[0]] = {"A": row[1], "B": row[2], "C": row[3], "D": row[4]}
    
p_usv_suelos = "/home/fidel/GitLab/numero-curva/data/usv_suelos.shp"
capa_usv_suelos = QgsVectorLayer(p_usv_suelos,"usv_suelos","ogr")

if 'nc' not in capa_usv_suelos.fields().names():
    capa_usv_suelos.dataProvider().addAttributes([QgsField('nc', QVariant.Int)])
    capa_usv_suelos.updateFields()

capa_usv_suelos.startEditing()
contador = 0
for f in capa_usv_suelos.getFeatures(): # recorrer los elementos de la capa
    cobertura = f['lc']
    suelo = f['tipo']
    f['nc'] = ncurva[cobertura][suelo]
    capa_usv_suelos.updateFeature(f)
    contador += 1
    if contador % 300 == 0: # cada 300 features print y save
        print(contador, cobertura, suelo, " -> ", ncurva[cobertura][suelo])
        capa_usv_suelos.commitChanges()
        capa_usv_suelos.startEditing()
        
capa_usv_suelos.commitChanges()
print("Números de curva computados...")


params = {'DATA_TYPE' : 0,
          'FIELD' :'nc',
          'EXTENT' : '276608.942600000,721404.277600000,1907923.729400000,2359266.936900000 [EPSG:32614]',
          'HEIGHT' : 100,
          'WIDTH' : 100,
          'INPUT' : '/home/fidel/GitLab/numero-curva/data/usv_suelos.shp',
          'INVERT' : False,
          'OUTPUT' : '/home/fidel/GitLab/numero-curva/raster_test2.tif',
          'UNITS' : 1 }

processing.run("gdal:rasterize", params)
print("Raster con los números de curva creado...")
print("Fin")


#ext = layer.extent()
#
#xmin = ext.xMinimum()
#xmax = ext.xMaximum()
#ymin = ext.yMinimum()
#ymax = ext.yMaximum()
#coords = "%f,%f,%f,%f" %(xmin, xmax, ymin, ymax)