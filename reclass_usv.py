
p_la_capa = "/home/fidel/GitLab/numero-curva/data/usv.shp"
la_capa = QgsVectorLayer(p_la_capa,"usv","ogr")

#agregar la columna "lc"
if 'lc' not in la_capa.fields().names():
    la_capa.dataProvider().addAttributes([QgsField('lc', QVariant.String)])
    la_capa.updateFields()

otros = []
cuales = {}
la_capa.startEditing()
contador = 0
for f in la_capa.getFeatures(): # recorrer los elementos de la capa
    contador += 1
    cat = f['DESCRIPCIO']
    if "SEC" in cat:
        su_tipo = "Vegetacion secundaria"
    elif "AGRI" in cat:
        su_tipo = "Agricultura"
    elif "BOS" in cat or "TUL" in cat or "HIDR" in cat:
        su_tipo = "Bosque"
    elif "MAT" in cat:
        su_tipo = "Matorral"
    elif "CUE" in cat:
        su_tipo = "Cuerpo de agua"
    elif "ASEN" in cat:
        su_tipo = "Zona urbana"
    elif "PAST" in cat or "PRA" in cat:
        su_tipo = "Pastizal"
    elif "SELV" in cat or "PAL" in cat:
        su_tipo = "Selva baja"
    elif "DESP" in cat or "SIN" in cat:
        su_tipo = "Suelo desnudo"
    else:
        su_tipo = "otros"
        if not cat in otros:
            otros.append(cat)
            
    if not su_tipo in cuales:
        cuales[su_tipo] = []
    if not cat in cuales[su_tipo]:
        cuales[su_tipo].append(cat)
    f['lc'] = su_tipo # asignar la categoria agregada a la columna "lc"
    if contador % 100 == 0: # cada 100 features salvar los cambios
        print(contador, cat, " -> ", su_tipo)
        la_capa.commitChanges()
        la_capa.startEditing()
    
    la_capa.updateFeature(f)
la_capa.commitChanges()

print(cuales)
pluma = open("/home/fidel/GitLab/numero-curva/categorias_agrupadas.txt","w")
for k, l in cuales.items():
    pluma.write(k+": "+", ".join(l)+"\n\n")
    
pluma.close()

