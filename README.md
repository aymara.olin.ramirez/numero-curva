# Curve number calculation

The runoff curve number (also called a curve number or simply CN) is an empirical parameter used in hydrology for predicting direct runoff or infiltration from rainfall excess. The curve number method was developed by the USDA Natural Resources Conservation Service, which was formerly called the Soil Conservation Service or SCS, the number is still popularly known as a "SCS runoff curve number" in the literature. The runoff curve number was developed from an empirical analysis of runoff from small catchments and hillslope plots monitored by the USDA. It is widely used and is an efficient method for determining the approximate amount of direct runoff from a rainfall event in a particular area. 


## Getting started
Edit paths acordingly and run 4 scripts inside QGIS 3, in order:

- reclass_usv.py
- reclass_suelo.py
- intersect_usv_suelos.py
- asigna_numero_curva.py

The steps are reclasify soil and cover vector layers, then intersect them, then calculate curve number for each intersection, then rasterize the result.

## Authors
M. en G. Fidel Serrano Candela, Laboratorio Nacional de Ciencias de la Sostenibilidad, 
Instituto de Ecología, Universidad Nacional Autónoma de México.

M. en C. Aymara Olin Ramírez González, Universidad Autónoma de México.


## License
### GNU GPL v3
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)    


