
p_la_capa = "/home/fidel/GitLab/numero-curva/data/eda_test.shp"
la_capa = QgsVectorLayer(p_la_capa,"suelos","ogr")

#agregar la columna "tipo"
if 'tipo' not in la_capa.fields().names():
    la_capa.dataProvider().addAttributes([QgsField('tipo', QVariant.String)])
    la_capa.updateFields()
    
la_capa.startEditing()

for f in la_capa.getFeatures(): # recorrer los elementos de la capa
    cat = f['NOM_SUE1']
    if cat:
        if "A" in cat:
            f['tipo'] = "A"
        elif "C" in cat or "F" in cat:
            f['tipo'] = "B"
        elif "L" in cat or "P" in cat or "R" in cat:
            f['tipo'] = "C"
        else:
            f['tipo'] = "D"
    else:
        f['tipo'] = "D"
    la_capa.updateFeature(f)
la_capa.commitChanges()